from __future__ import annotations
from dataclasses import dataclass
import sys

from inigrep.cli import Action, HelpNeeded
from inigrep.cli import UsageError as IGUsageError
from inigrep.front import UnitIni

from .config import ConfigRepo
from .common import BugError, UsageError


@dataclass
class Args:
    args: list[str]

    def _check_args(self,
                    num: int = 1,
                    name: str = '',
                    *ctx_names: str,
                    ) -> None:
        if len(self.args) >= num:
            # we have enough
            return
        ctx_hint = '' if not ctx_names else f" after `{' '.join(ctx_names)}`"
        raise UsageError(
            f"no {name}{ctx_hint}?"
            if name else
            f"missing one or more arguments{ctx_hint}"
        )

    def len(self) -> int:
        return len(self.args)

    def take1(self, name: str = '') -> str:
        self._check_args(1, name)
        return self.args.pop(0)

    def next(self) -> str:
        return self.args[0]

    def next_is(self, *cases) -> bool:
        return self.args[0] in cases

    def next_was(self, *cases) -> bool:
        if not self.next_is(*cases):
            return False
        self.args.pop(0)
        return True

    def rest(self) -> list[str]:
        return self.args[:]


@dataclass
class Options:
    action: Action = Action.NONE
    oneline: bool = False
    query: str | None = None
    filename: str | None = None


def parse_args(args: list[str]) -> Options:
    o = Options()
    a = Args(args)
    while a.args:
        if a.next_was('--help'):
            raise HelpNeeded()
        elif a.next_was('-1'):
            o.oneline = True
            a.take1()
        elif a.next_was('-e', '--basic'):
            o.action = Action.VALUES
            o.query = a.take1()
            if a.args:
                o.filename = a.take1()
        elif a.next_was('-r', '--raw'):
            o.action = Action.RAW_VALUES
            o.query = a.take1()
            if a.args:
                o.filename = a.take1()
        elif a.next_was('-K', '--lskey'):
            o.action = Action.LIST_KEYS
            o.query = a.take1()
            if a.args:
                o.filename = a.take1()
        elif a.next_was('-S', '--lssct'):
            o.action = Action.LIST_SECTIONS
            o.filename = a.take1()
        elif a.next_was('-P', '--lspth'):
            o.action = Action.LIST_PATHS
            o.filename = a.take1()
        elif a.next_was('-c', '--clone'):
            o.action = Action.CLONE
            o.filename = a.take1()
        elif a.next().startswith('-'):
            raise UsageError('unknown argument: %s' % a.next())
        else:
            break
    if o.action == Action.NONE:
        o.action = Action.VALUES
        o.query = a.take1()
        if a.args:
            o.filename = a.take1()
    if a.args:
        raise UsageError(f"too many arguments: {a.args}")
    return o


def run(options: Options, conf_repo: ConfigRepo):
    def select_files() -> list[str]:
        if options.filename:
            return [str(p) for p in conf_repo.find(options.filename)]
        if options.query:
            basename = options.query.split('.', maxsplit=1)[0]
            filename = f'{basename}{conf_repo.suffix}'
            return [str(p) for p in conf_repo.find(filename)]
        raise BugError("could not determine files to load")
    ini = UnitIni.from_files(select_files())
    if options.action == Action.VALUES:
        return ini.values(options.query)
    if options.action == Action.RAW_VALUES:
        return ini.raw_values(options.query)
    if options.action == Action.LIST_KEYS:
        return ini.list_keys(options.query)
    if options.action == Action.LIST_PATHS:
        return ini.list_paths()
    if options.action == Action.LIST_SECTIONS:
        return ini.list_sections()
    if options.action == Action.CLONE:
        return ini.clone()
    raise ValueError(f"invalid action: {options.action}")


def run_conf_builtin(conf_repo: ConfigRepo,
                     args: list[str],
                     ) -> int:

    try:
        options = parse_args(args)
    except IGUsageError as e:
        raise UsageError(e)
    except HelpNeeded as e:
        sys.stderr.write('%s\n' % __doc__)
        sys.stderr.write('%s\n' % e)
        return 0

    gen = run(options, conf_repo)
    for line in gen:
        print(line)
        if options.oneline:
            break
    return 0
