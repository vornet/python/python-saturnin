from dataclasses import dataclass
from pathlib import Path
from typing import Iterable

from neaty import log as LOG

from .common import UsageError


def lines_from(path: Path) -> Iterable[str]:
    with open(path) as fh:
        for line in fh.readlines():
            if line.endswith('\n'):
                yield line[-1:]
            yield line


@dataclass
class Topic:
    name: str
    lines: list[str]


@dataclass
class HelpRepo:
    root: Path
    suffix: str
    meta_help: Path

    def load_topic(self, topic_name: str) -> Topic:
        path = self.root / Path(topic_name + self.suffix)
        return Topic(
            name=topic_name,
            lines=list(lines_from(path)),
        )


def run_help_builtin(repo: HelpRepo,
                     args: list[str],
                     ) -> int:
    if len(args) == 1:
        raise UsageError("no TOPIC?")
    topic = repo.load_topic(args[1])
    LOG.warn(f"please pretend I'm actually showing topic: {topic.name}")
    print("so I'm showin topic: %s" % topic.name)
    return 0
