#!/usr/bin/python3

from __future__ import annotations
import os
import itertools
from typing import ClassVar, Iterable
from dataclasses import dataclass
from pathlib import Path
from enum import Enum

import inigrep.core
import inigrep.front
import neaty.log

from .common import ParamSpec, mkpath

LOG = neaty.log


@dataclass
class Dirstack:
    dirs: list[Path]

    @classmethod
    def from_paths(cls,
                   paths: list[str],
                   ) -> Dirstack:

        def find_roots():
            for candidate in paths:
                if not candidate:
                    continue
                path = mkpath(candidate)
                if not path.is_dir():
                    continue
                yield path

        def find_confdirs():
            for root in find_roots():
                if root.parts[-1] == 'ini.d':
                    for subdir in root.iterdir():
                        if not subdir.is_dir():
                            continue
                        yield subdir
                else:
                    yield root

        confdirs = list(find_confdirs())
        LOG.debugv('confdirs', confdirs)
        return cls(dirs=confdirs)

    @classmethod
    def from_envs(cls,
                  envs: str,
                  ) -> Dirstack:
        paths = [Path(p)
                 for p in envs.split(':')
                 if p]
        return cls(paths)

    def __str__(self):
        return ':'.join(str(d) for d in self.dirs)

    def find(self,
             subpath: str,
             ) -> list[Path]:
        """
        Find all existing instances of sub-path *subpath* on this Dirstack

        Go through all elements of Dirstack instance, looking for file or
        directory, whose path is formed by adding SUBPATH after the element.
        Return each existing path, ignore rest.

        For example, with following setup:

            # mkdir -p foo/one bar/one
            # mkdir -p bar/two/slashes
            c = Dirstack.from_paths('foo:bar:baz'.split(':'))

        call

            c.find('one')

        would return `['foo/one', 'bar/one']`, while

            c.find('two/slashes')

        would return `['bar/two/slashes']`.
        """
        trydirs = [d for d in self.dirs
                   if d.is_dir()]
        out = []
        for trydir in trydirs:
            LOG.debugv('trydir', trydir)
            trypath = trydir / Path(subpath)
            if trypath.exists():
                LOG.debugv('exists: trypath', trypath)
                out.append(trypath)
        return out


class Strategy(Enum):
    FIRST = 'first'
    JOIN = 'join'


@dataclass
class MetafileConfigRepo:
    filename: str
    dirstack: Dirstack
    unit_ini: inigrep.front.UnitIni

    @classmethod
    def from_env(cls, filename: str | None = None):
        def collect_lines(dirstack: Dirstack, filename: str) -> Iterable[str]:
            paths = dirstack.find(filename)
            LOG.debugv('paths', paths)
            for path in paths:
                shortpath = str(path).replace('$HOME', os.environ['HOME'])
                yield "# file: %s" % shortpath
                with open(path) as fp:
                    for line in fp.readlines():
                        yield line[:-1]
        dirstack = Dirstack.from_envs(
            os.environ.get('_SATURNIN_CONF_PATH', '')
        )
        filename = filename or (
            os.environ['_SATURNIN_SUBCOMMAND']
            + os.environ.get('_SATURNIN_CONF_SUFFIX', '')
        )
        return cls(
            filename=filename,
            dirstack=dirstack,
            unit_ini=inigrep.front.UnitIni.from_lines(collect_lines(
                dirstack=dirstack,
                filename=filename
            ))
        )

    def _data(self) -> dict[str, list[str]]:
        return {
            str(key): [str(v) for v in values]
            for key, values in self.unit_ini.data().items()
        }

    def _raw_data(self) -> dict[str, list[str]]:
        return {
            str(key): [str(v) for v in values]
            for key, values in self.unit_ini.raw_data().items()
        }

    def clone(self) -> Iterable[str]:
        """
        Return lines of INI file with the same data as in this object
        """
        return map(str, self.unit_ini.clone())

    def data(self, prefix: str = "") -> dict[str, list[str]]:
        """
        Get all data matching keypath *prefix* as a dictionary

        Return dictionary mapping all keypaths matching *prefix* to
        lists of their values.  If specified, returned dictionary keys
        will have *prefix* part removed.
        """
        key_prefix = prefix.rstrip('.') + '.' if prefix else ''
        return {
            key.removeprefix(key_prefix): [str(v) for v in values]
            for key, values
            in self._data().items()
            if key.startswith(key_prefix)
        }

    def linemap(self, prefix: str = "") -> inigrep.front.LineMap:
        """
        Like .data() but return LineMap with convenience access methods
        """
        return inigrep.front.LineMap(data=self.data(prefix=prefix), ns=prefix)

    def list_keys(self, section: str) -> Iterable[str]:
        """
        Return list of keys under *section*.
        """
        return map(str, self.unit_ini.list_keys(section))

    def list_paths(self) -> Iterable[str]:
        """
        Return list of all paths
        """
        return map(str, self.unit_ini.list_paths())

    def list_sections(self) -> Iterable[str]:
        """
        Return list of all sections
        """
        return map(str, self.unit_ini.list_sections())

    def raw_data(self, prefix: str = "") -> dict[str, list[str]]:
        """
        Get all data matching keypath *prefix* as a dictionary

        Same as .data(), but keeps in-line comments and leading/trailing
        whitespace around values.
        """
        key_prefix = prefix.rstrip('.') + '.' if prefix else ''
        return {
            key.removeprefix(key_prefix): [str(v) for v in values]
            for key, values
            in self._raw_data().items()
            if key.startswith(key_prefix)
        }

    def raw_values(self, kpath: str) -> Iterable[str]:
        """
        Return list of raw values at key path *kpath*.

        Same as .values(), but keeps in-line comments and leading/trailing
        whitespace around values.
        """
        return map(str, self.unit_ini.raw_values(kpath))

    def raw_linemap(self, prefix: str = "") -> inigrep.front.LineMap:
        """
        Like .raw_data() but return LineMap with convenience access methods
        """
        return inigrep.front.LineMap(data=self.data(prefix=prefix), ns=prefix)

    def values(self, kpath: str) -> Iterable[str]:
        """
        Return list of values at key path *kpath*.
        """
        return map(str, self.unit_ini.values(kpath))


@dataclass
class ConfigRepo:
    dirstack: Dirstack
    suffix: str

    env_specs: ClassVar[list[ParamSpec]] = [
        ParamSpec(attr='dirstack', envvar='_SATURNIN_CONF_PATH', factory=Dirstack.from_envs),
        ParamSpec(attr='suffix', envvar='_SATURNIN_CONF_SUFFIX'),
    ]

    @classmethod
    def from_env(cls):
        dirstack = Dirstack.from_envs(
            os.environ.get('_SATURNIN_CONF_PATH', '')
        )
        suffix = os.environ.get('_SATURNIN_CONF_SUFFIX', '')
        return cls(dirstack, suffix)

    def _load(self,
              filenames: list[str],
              strategy: Strategy,
              ) -> Iterable[inigrep.core.LineT]:
        """
        Print contents of *files*

        Each argument means possible file candidate.  If candidate contains
        slash, it's treated as file path and is printed directly.  If it's
        single dash, standard input is copied.

        In all other cases, filename is searched in all elements of variable
        _SATURNIN_CONF_PATH; output then depends on chosen $Strategy: with
        'first' strategy, first existing file is printed, with 'join'
        strategy, all existing files are printed.
        """
        readers = []
        for filename in filenames:
            if filename == '-' or '/' in filename:
                # stdin, or path (with slash)
                readers.append(inigrep.core.FileReader([filename]))
            else:
                # name given, find all its incarnations
                paths = self.dirstack.find(filename)
                LOG.debugv('paths', paths)
                readers.append(self._merge(paths, strategy))
        for line in itertools.chain(*readers):
            yield line

    def _merge(self,
               paths: list[Path],
               strategy: Strategy,
               ) -> Iterable:
        '''
        Take paths and applying merge strategy, return inigrep reader
        '''
        if not paths:
            return inigrep.core.FileReader([])
        if strategy == Strategy.FIRST:
            return inigrep.core.FileReader([str(paths[0])])
        if strategy == Strategy.JOIN:
            def reader():
                for path in paths:
                    shortpath = str(path).replace('$HOME', os.environ['HOME'])
                    yield "# file: %s" % shortpath
                    for line in inigrep.core.FileReader([str(path)]):
                        yield line
            return reader()
        raise ValueError("unsupported strategy: %r" % strategy)

    def conf_filenames(self
                       ) -> list[str]:
        """
        Print all conf files found at $_SATURNIN_CONF_PATH

        Print list of filenames usable as FNAME argument to saturnin__conf().
        """
        trydirs = [d for d in self.dirstack.dirs
                   if d.is_dir()]
        out = []
        for trydir in trydirs:
            for name in trydir.iterdir():
                sname = str(name)
                if sname.endswith(self.suffix):
                    if sname not in out:
                        out.append(sname)
        return out

    def data(self,
             prefix: str = "",
             filename: str | None = None,
             strategy: Strategy = Strategy.JOIN,
             ) -> dict[str, list[str]]:
        """
        Get all data matching keypath *prefix* as a dictionary

        Return dictionary mapping all keypaths matching *prefix* to
        lists of their values.  The returned dictionary keys will have
        *prefix* part removed.
        """
        def load_data():
            real_fn = filename or prefix.split('.', 1)[0] + self.suffix
            reader = self._load([real_fn], strategy)
            for k, v in inigrep.core._r_data(reader).items():
                if not k.startswith(key_prefix):
                    continue
                yield k, v
        key_prefix = prefix.rstrip('.') + '.' if prefix else ''
        if not filename and not prefix:
            raise ValueError("must provide either prefix or filename")
        return {
            key.removeprefix(key_prefix): [str(v) for v in values]
            for key, values
            in load_data()
        }

    def raw_data(self,
                 prefix: str = "",
                 filename: str | None = None,
                 strategy: Strategy = Strategy.JOIN,
                 ) -> dict[str, list[str]]:
        """
        Get all raw data matching keypath *prefix* as a dictionary

        Return dictionary mapping all keypaths matching *prefix* to
        lists of their raw values.  The returned dictionary keys will have
        *prefix* part removed.
        """
        def load_data():
            real_fn = filename or prefix.split('.', 1)[0] + self.suffix
            reader = self._load([real_fn], strategy)
            for k, v in inigrep.core._r_raw_data(reader).items():
                if not k.startswith(key_prefix):
                    continue
                yield k, v
        if not filename and not prefix:
            raise ValueError("must provide either prefix or filename")
        key_prefix = prefix.rstrip('.') + '.' if prefix else ''
        return {
            key.removeprefix(key_prefix): [str(v) for v in values]
            for key, values
            in load_data()
        }

    def find(self,
             subpath: str,
             ) -> list[Path]:
        """
        Find existing *subpath* under set of config directories.
        """
        return self.dirstack.find(subpath)

    def list_keys(self,
                  section: str,
                  filename: str | None = None,
                  strategy: Strategy = Strategy.JOIN,
                  ) -> list[str]:
        """
        Key listing engine
        """
        filename = filename or section.split('.', 1)[0] + self.suffix
        reader = self._load([filename], strategy)
        return list(inigrep.core._r_list_keys(reader, section))

    def list_paths(self,
                   filename: str | None = None,
                   strategy: Strategy = Strategy.JOIN,
                   ) -> list[str]:
        """
        Key path listing engine
        """
        filenames = self.conf_filenames()
        if filename is not None:
            filenames = [filename]
        reader = self._load(filenames, strategy)
        return list(inigrep.core._r_list_paths(reader))

    def list_sections(self,
                      filename: str | None = None,
                      strategy: Strategy = Strategy.JOIN,
                      ) -> list[str]:
        """
        Section listing engine
        """
        filenames = self.conf_filenames()
        if filename is not None:
            filenames = [filename]
        reader = self._load(filenames, strategy)
        return list(inigrep.core._r_list_sections(reader))

    def raw_values(self,
                   kpath: str,
                   filename: str | None = None,
                   strategy: Strategy = Strategy.JOIN,
                   ) -> list[str]:
        """
        Raw inigrep engine (keep comments and value whitespace)
        """
        filename = filename or kpath.split('.', 1)[0] + self.suffix
        reader = self._load([filename], strategy)
        return list(inigrep.core._r_raw_values(reader, kpath))

    def values(self,
               kpath: str,
               filename: str | None = None,
               strategy: Strategy = Strategy.JOIN,
               ) -> list[str]:
        """
        Basic engine
        """
        filename = filename or kpath.split('.', 1)[0] + self.suffix
        reader = self._load([filename], strategy)
        return list(inigrep.core._r_values(reader, kpath))
