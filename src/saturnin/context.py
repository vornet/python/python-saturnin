#!/usr/bin/python3

from typing import Optional
from dataclasses import dataclass
import os

from saturnin.config import MetafileConfigRepo
from saturnin.common import AppInfo, from_env


@dataclass
class Context:
    subcommand: str
    metacommand: str
    usage_name: str
    appinfo: AppInfo


def get_context() -> Context:
    subcommand = os.environ.get('_SATURNIN_SUBCOMMAND', '')
    metacommand = os.environ.get('_SATURNIN_METACOMMAND', '')
    appinfo: AppInfo = from_env(AppInfo)            # type: ignore
    return Context(
        subcommand=metacommand,
        metacommand=subcommand,
        usage_name=f"{metacommand} [{metacommand}-options] {subcommand}",
        appinfo=appinfo,
    )


def get_config_repo(filename: Optional[str] = None) -> MetafileConfigRepo:
    return MetafileConfigRepo.from_env(filename)
