#!/usr/bin/python3

from dataclasses import dataclass
from pathlib import Path
from typing import Any, Callable, ClassVar, Iterable, Optional, Protocol, runtime_checkable
import os

import neaty.log

LOG = neaty.log


class BugError(RuntimeError):
    pass


class BinpathError(RuntimeError):
    pass


class UsageError(RuntimeError):
    pass


def mkpath(P: str) -> Path:
    return Path(os.path.expandvars(P))


@dataclass
class ParamSpec:
    attr: str
    envvar: str
    factory: Optional[Callable[[str], Any]] = None


@runtime_checkable
class PassableP(Protocol):
    env_specs: ClassVar[list[ParamSpec]]


@runtime_checkable
class PassableTypeP(Protocol):
    env_specs: list[ParamSpec]


APPINFO_SPECS = [
    ParamSpec(attr='cache_home', envvar='_SATURNIN_CACHE_HOME', factory=mkpath),
    ParamSpec(attr='codename', envvar='_SATURNIN_APP_CODENAME'),
    ParamSpec(attr='config_home', envvar='_SATURNIN_CONFIG_HOME', factory=mkpath),
    ParamSpec(attr='data_home', envvar='_SATURNIN_DATA_HOME', factory=mkpath),
    ParamSpec(attr='git_hash', envvar='_SATURNIN_APP_GIT_HASH'),
    ParamSpec(attr='git_summary', envvar='_SATURNIN_APP_GIT_SUMMARY'),
    ParamSpec(attr='license', envvar='_SATURNIN_APP_LICENSE'),
    ParamSpec(attr='maintainer', envvar='_SATURNIN_APP_MAINTAINER'),
    ParamSpec(attr='name', envvar='_SATURNIN_APP_NAME'),
    ParamSpec(attr='pkgname', envvar='_SATURNIN_APP_PKGNAME'),
    ParamSpec(attr='tagline', envvar='_SATURNIN_APP_TAGLINE'),
    ParamSpec(attr='url', envvar='_SATURNIN_APP_URL'),
    ParamSpec(attr='version', envvar='_SATURNIN_APP_VERSION'),
]


def to_env_items(obj: PassableP) -> Iterable[tuple[str, str]]:
    for spec in obj.__class__.env_specs:
        value = getattr(obj, spec.attr)
        if value is None:
            continue
        yield spec.envvar, str(value)


def from_env(cls: PassableTypeP) -> PassableP:
    obj = obj_from_env(cls.env_specs, cls)
    assert isinstance(obj, PassableP)
    return obj


def obj_from_env(specs: list[ParamSpec], cls) -> object:
    def items():
        for spec in specs:
            if spec.envvar not in os.environ:
                continue
            raw_value = os.environ[spec.envvar]
            value = spec.factory(raw_value) if spec.factory else raw_value
            yield spec.attr, value
    kwargs = {k: v for k, v in items()}
    return cls(**kwargs)


@dataclass
class AppInfo:
    cache_home: Path
    config_home: Path
    data_home: Path
    codename: str = ''
    git_hash: str = ''
    git_summary: str = ''
    license: str = ''
    maintainer: str = ''
    name: str = ''
    pkgname: str = ''
    tagline: str = ''
    url: str = ''
    version: str = '0.0.0+_no_version_'

    env_specs: ClassVar[list[ParamSpec]] = APPINFO_SPECS

    @property
    def about(self) -> str:
        """
        Human-readable version info

        Basic version info is already stored in *version* property;
        this is a more descriptive version info intended for humans.
        """
        def parts():
            yield self.name
            if self.tagline:
                yield f"({self.tagline})"
            yield self.version
            if self.codename:
                yield "-"
                yield self.codename
        return ' '.join(parts())
