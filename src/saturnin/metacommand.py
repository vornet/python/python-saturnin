#!/usr/bin/python3

from __future__ import annotations
from dataclasses import dataclass, field
from enum import Enum
from pathlib import Path
from typing import Any, Iterable

import os
import subprocess
import sys

import neaty.log

from .common import (
    AppInfo,
    BugError,
    BinpathError,
    UsageError,
    mkpath,
    to_env_items,
)
from .config import (
    ConfigRepo,
    Dirstack,
)
from ._meta import (
    CODENAME,
    TAGLINE,
    VERSION,
)
from .conf_builtin import run_conf_builtin
from .help_builtin import (
    run_help_builtin,
    HelpRepo,
)

LOG = neaty.log


class Action(Enum):
    NONE = 'none'
    SHOW_HELP = 'show_help'
    SHOW_VERSION = 'show_version'
    SHOW_ABOUT = 'show_about'
    USAGE_ERROR = 'usage_error'
    RUN_SUBCOMMAND = 'run_subcommand'
    RUN_BUILTIN = 'run_builtin'
    USAGE_ERROR_UNKNOWN_ARGUMENT = 'usage_error_unknown_argument'
    USAGE_ERROR_NOSUCH_SUBCOMMAND = 'usage_error_nosuch_subcommand'
    SATURNIN_DEVEL_CMD = 'run_python'


class Builtin(Enum):
    CONF = 'conf'
    HELP = 'help'


class MsgMode(Enum):
    COLOR = 'color'
    PLAIN = 'plain'
    AUTO = 'auto'


@dataclass
class LibexecRepo:
    root: Path
    prefix: str

    def get_binpath(self, name: str) -> Path:
        path = Path(self.root / (self.prefix + name))
        if not path.is_file():
            raise BinpathError(name)
        return path

    def list_commands(self) -> Iterable[str]:
        for path in self.root.iterdir():
            if not path.is_file():
                continue
            if not path.name.startswith(self.prefix):
                continue
            if not os.access(path, os.X_OK):
                continue
            yield path.name.removeprefix(self.prefix)


@dataclass
class CompletionRepo:
    path: Path


KwArgsT = dict[str, Any]
ActionArgsT = list[str]


def parse_argv(argv: list[str],
               allowed_builtins: set[Builtin],
               ) -> tuple[KwArgsT, Action, ActionArgsT]:

    def is_allowed_builtin(value):
        return any([
            value == bltn.value
            for bltn in allowed_builtins
        ])

    args_togo = argv.copy()
    if not argv:
        raise BugError("no argv to parse?")
    parsed: dict[str, Any] = {}
    parsed['mc_binname'] = args_togo.pop(0).split('/')[-1]
    while args_togo:
        arg = args_togo.pop(0)
        if False:
            pass
        elif arg in ["--saturnin-devel-cmd"]:
            return parsed, Action.SATURNIN_DEVEL_CMD, args_togo or ['python3']
        elif arg in ["-C", "--no-color"]:
            parsed['msgmode'] = MsgMode.PLAIN
        elif arg in ["-V"]:
            return parsed, Action.SHOW_VERSION, []
        elif arg in ["-c", "--color"]:
            parsed['msgmode'] = MsgMode.COLOR
        elif arg in ["-d", "--debug"]:
            parsed['debug'] = True
        elif arg in ["-D", "--full-debug"]:
            parsed['debug'] = True
        elif arg in ["-h", "--help"]:
            return parsed, Action.SHOW_HELP, []
        elif arg in ["--python-profiling"]:
            parsed['profiling_enabled'] = True
        elif arg in ["-q", "--quiet"]:
            parsed['verbose'] = False
        elif arg in ["-v", "--verbose"]:
            parsed['verbose'] = True
        elif arg in ["--version"]:
            return parsed, Action.SHOW_ABOUT, []
        elif arg.startswith('-'):
            return parsed, Action.USAGE_ERROR_UNKNOWN_ARGUMENT, [arg]
        elif is_allowed_builtin(arg):
            return parsed, Action.RUN_BUILTIN, [arg] + args_togo
        else:
            return parsed, Action.RUN_SUBCOMMAND, [arg] + args_togo
    return parsed, Action.NONE, []


@dataclass
class VerbosityParams:
    debug: bool = False
    verbose: bool = False
    msgmode: MsgMode = MsgMode.AUTO


@dataclass
class MetaCommand:
    mc_binname: str
    appinfo: AppInfo
    help_repo: HelpRepo
    completion_repo: CompletionRepo
    libexec_repo: LibexecRepo
    conf_repo: ConfigRepo
    action: Action = Action.NONE
    action_args: list[str] = field(default_factory=list)
    verbose: bool = False
    msgmode: MsgMode = MsgMode.AUTO
    debug: bool = False
    profiling_enabled: bool = False

    def run(self) -> None:
        try:
            sys.exit(self.run_action())
        except BinpathError as e:
            LOG.debugv('e', e)
            self.usage_exit(hint=f"no such command: {e}")
        except UsageError as e:
            LOG.debugv('e', e)
            self.usage_exit(hint=str(e))

    def usage_exit(self, hint) -> None:
        lines = [
            "usage:",
            f"  {self.mc_binname} [options] COMMAND [ARG...]",
            "",
            "options:",
            "  -C, --no-color   disable ANSI colors",
            "  -D, --full-debug   turn on gory debugging",
            "  -V, --version  show version and exit",
            "  -c, --color    force ANSI colors",
            "  -d, --debug    turn on debugging",
            "  -h, --help     show this help message and exit",
            "  -q, --quiet    turn off verbosity",
            "  -v, --verbose  turn on verbosity",
            "",
            "commands:",
        ] + [
            '  ' + cmd for cmd in sorted(self.libexec_repo.list_commands())
        ]
        LOG.mkusage('\n'.join(lines))
        if hint:
            LOG.warn("")
            LOG.warn(hint)
        sys.exit(2)

    def run_action(self) -> int:
        a = self.action
        args = self.action_args
        if a == Action.NONE:
            raise UsageError("no subcommand provided")
        if a == Action.USAGE_ERROR_NOSUCH_SUBCOMMAND:
            raise UsageError(f"no such subcommand: {args[0]}")
        if a == Action.USAGE_ERROR_UNKNOWN_ARGUMENT:
            raise UsageError(f"unknown argument: {args[0]}")
        if a == Action.SHOW_VERSION:
            print(self.appinfo.version)
            return 0
        if a == Action.SHOW_HELP:
            LOG.warn("cannot help you yet, sorry")
            return 4
        if a == Action.SHOW_ABOUT:
            print(self.about)
            return 0
        if a == Action.RUN_BUILTIN:
            if args[0] == 'conf':
                return run_conf_builtin(self.conf_repo, args[1:])
            if args[0] == 'help':
                return run_help_builtin(self.help_repo, args[1:])
            else:
                raise BugError(f"invalid builtin in {self.appinfo.version}")
        if a == Action.SATURNIN_DEVEL_CMD:
            return subprocess.call(
                args,
                env=dict(self.subcommand_env_items(command='_saturnin_devel_cmd_'))
            )
        if a == Action.RUN_SUBCOMMAND:
            return self.run_subcommand(command=args[0], args=args[1:])
        raise BugError(f"invalid action: {a} in {self.appinfo.version}")

    def subcommand_env_items(self, command: str) -> Iterable[tuple[str, str]]:
        yield from os.environ.items()
        yield from to_env_items(self.appinfo)
        yield from to_env_items(self.conf_repo)
        yield '_SATURNIN_METACOMMAND', self.mc_binname
        yield '_SATURNIN_SUBCOMMAND', command
        yield 'NEATY_USAGE', 'subcommand'
        yield 'PRETTY_USAGE', 'subcommand'
        if self.debug:
            yield 'NEATY_DEBUG', 'true'
            yield 'PRETTY_DEBUG', 'true'
        if self.verbose:
            yield 'NEATY_VERBOSE', 'true'
            yield 'PRETTY_VERBOSE', 'true'

    def run_subcommand(self,
                       command: str,
                       args: list[str],
                       ) -> int:
        def _maybe_profiling() -> list[str]:
            if not self.profiling_enabled:
                return []
            Path.mkdir(self.appinfo.cache_home, parents=True, exist_ok=True)
            outfile = self.appinfo.cache_home / 'saturnin-profiling.out'
            LOG.warn(f"profiling info will be saved to: {outfile}")
            return [
                'python3',
                '-m' 'cProfile',
                '-o', str(outfile),
            ]
        binpath = self.libexec_repo.get_binpath(command)
        cmd = _maybe_profiling() + [str(binpath)] + args
        LOG.debugv('cmd', cmd)
        return subprocess.call(
            cmd,
            env=dict(self.subcommand_env_items(command)),
        )

    @property
    def about(self) -> str:
        """
        Print human-readable version info

        Basic version info is already stored in *version* property;
        this function prints more descriptive paragraph including Saturnin's
        own version.
        """
        return '\n'.join([
            self.appinfo.about,
            f"Powered by Saturnin ({TAGLINE}) {VERSION} - {CODENAME}",
        ])


def parse_bool(text) -> bool | None:
    if text == 'true':
        return True
    if text == 'false':
        return False
    raise ValueError(f"invalid bool value: {text!r}")


def collect_default_options(verbose: str,
                            msgmode: str,
                            ):
    if verbose:
        yield 'verbose', parse_bool(verbose)
    if msgmode:
        yield 'msgmode', MsgMode(msgmode)


def main(argv: list[str],
         app_codename: str,
         app_git_hash: str,
         app_git_summary: str,
         app_license: str,
         app_maintainer: str,
         app_name: str,
         app_pkgname: str,
         app_tagline: str,
         app_url: str,
         app_version: str,
         builtins: str,
         cache_home: str,
         config_home: str,
         data_home: str,
         conf_path: list[str],
         conf_suffix: str,
         help_topicpath: str,
         help_topicsuffix: str,
         libexec: str,
         libexec_prefix: str,
         meta_help: str,
         sccompgen_path: str,
         verbose: str,
         msgmode: str,
         ):
    appinfo = AppInfo(
        cache_home=mkpath(cache_home),
        codename=app_codename,
        config_home=mkpath(config_home),
        data_home=mkpath(data_home),
        git_hash=app_git_hash,
        git_summary=app_git_summary,
        license=app_license,
        maintainer=app_maintainer,
        name=app_name,
        pkgname=app_pkgname,
        tagline=app_tagline,
        url=app_url,
        version=app_version,
    )
    LOG.debugv('main():appinfo', appinfo)
    mc_kwargs: KwArgsT = dict(collect_default_options(
        verbose=verbose,
        msgmode=msgmode,
    ))
    parsed_options, action, action_args = parse_argv(
        argv=argv,
        allowed_builtins=set(Builtin(b.strip()) for b in builtins.split()),
    )
    mc_kwargs.update(
        appinfo=appinfo,
        libexec_repo=LibexecRepo(
            root=mkpath(libexec),
            prefix=libexec_prefix,
        ),
        conf_repo=ConfigRepo(
            dirstack=Dirstack.from_paths(conf_path),
            suffix=conf_suffix,
        ),
        help_repo=HelpRepo(
            root=Path(help_topicpath),
            suffix=help_topicsuffix,
            meta_help=Path(meta_help),
        ),
        completion_repo=CompletionRepo(
            path=Path(sccompgen_path)
        ),
        action=action,
        action_args=action_args,
    )
    LOG.debugv('main():mc_kwargs', mc_kwargs)
    mc_kwargs.update(parsed_options)
    app = MetaCommand(**mc_kwargs)
    LOG.mode.verbose = app.verbose
    LOG.mode.debug = app.debug
    LOG.debugv('main():app', app)
    app.run()
